﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BrowserBridge : MonoBehaviour
{
    int w = 0;
    int h = 0;

    void Awake()
    {
        Application.ExternalEval("SendMessage(\"BrowserBridge\", \"SetResolution\", window.innerWidth + \"x\" + window.innerHeight);");
    }

    public void SetResolution(string res)
    {
        string[] fields = res.Split('x');
        if (fields.Length != 2)
            return;

        w = int.Parse(fields[0]);
        h = int.Parse(fields[1]);

        Screen.SetResolution(w, h, false);
    }
}
