﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;

public class Pawn : MonoBehaviour, IDamageable
{
    public int MaxHealth = 100;
    public int StartHealth = 100;
    private int _iHealth;
    public int Health
    {
        get { return _iHealth; }
        set
        {
            _iHealth = value;

            if (OnHealthChanged != null) OnHealthChanged(this);
        }
    }
    public bool DestroyOnKilled = true;
    public AudioSource FootstepSound;
    private float _fFootstepNext = 1f;

    public delegate void PawnEvent(Pawn sender, params object[] args);
    public PawnEvent OnPossess;
    public PawnEvent OnUnPossess;
    public PawnEvent OnEquip;
    public PawnEvent OnUnequip;
    public PawnEvent OnHealthChanged;
    public PawnEvent OnTakeDamage;
    public PawnEvent OnKilled;
    public float WalkSpeed = 1.5f;
    public float RunSpeed = 3f;
    public float CurrentSpeed = 1.5f;
    private Vector3 _vLastPosition;
    public float MoveDelta
    {
        get { return Vector3.Magnitude(transform.position - _vLastPosition); }
    }

    private CharacterController _characterController;
    public CharacterController CharacterController
    {
        get { return _characterController; }
    }

    private UnityEngine.AI.NavMeshAgent _navMeshAgent;
    public UnityEngine.AI.NavMeshAgent NavMeshAgent
    {
        get { return _navMeshAgent; }
    }

    private PawnController _controller;
    public PawnController Controller
    {
        get { return _controller; }
    }

    public float BaseMoveSpeed = 1f;
    public Transform HeadTransform;

    private Equipable _equipedItem;
    public Equipable EquipedItem
    {
        get { return _equipedItem; }
    }

    public GameObject DefaultEquiped;

    private Collider[] _ownColliders;

    protected virtual void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        _navMeshAgent = GetComponent<NavMeshAgent>();

        if (DefaultEquiped)
        {
            Equipable equipable = DefaultEquiped.GetComponent<Equipable>();

            if (equipable)
                Equip(equipable);
        }

        // Bind all hurt collisions to this
        HurtCollision hurtLocal = GetComponent<HurtCollision>();
        if (hurtLocal)
            hurtLocal.Damageable = this;

        HurtCollision[] hurt = GetComponentsInChildren<HurtCollision>();
        for (int i = 0; i < hurt.Length; i++)
            hurt[i].Damageable = this;

        _iHealth = StartHealth;

        _ownColliders = GetComponentsInChildren<Collider>();
    }

    protected virtual void OnEnable()
    {

    }

    protected virtual void OnDisable()
    {

    }

    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {
        // Apply gravity
        if (CharacterController && !NavMeshAgent)
            CharacterController.Move(Physics.gravity * Time.deltaTime);

        if (NavMeshAgent)
            NavMeshAgent.speed = CurrentSpeed;

        _fFootstepNext -= MoveDelta * 70f * Time.deltaTime;
        if (_fFootstepNext <= 0f)
        {
            FootstepSound.Play();
            _fFootstepNext = 1f;
        }

        _vLastPosition = transform.position;
    }

    protected virtual void FixedUpdate()
    {

    }

    protected virtual void LateUpdate()
    {

    }

    public virtual void Possess(PawnController controller)
    {
        if (Controller)
            if (OnUnPossess != null) OnUnPossess(this, Controller);

        _controller = controller;

        if (Controller)
            if (OnPossess != null) OnPossess(this, Controller);
    }

    public virtual void MoveForward(float speed)
    {
        if (speed == 0f) return;

        float spd = speed * CurrentSpeed * Time.deltaTime;

        if (CharacterController)
            CharacterController.Move(transform.forward * spd);

        if (NavMeshAgent)
            NavMeshAgent.Move(transform.forward * spd);
    }

    public virtual void MoveRight(float speed)
    {
        if (speed == 0f) return;

        float spd = speed * CurrentSpeed * Time.deltaTime;

        if (CharacterController)
            CharacterController.Move(transform.right * spd);

        if (NavMeshAgent)
            NavMeshAgent.Move(transform.right * spd);
    }

    public virtual void MoveTo(Vector3 destination)
    {
        if (NavMeshAgent)
            NavMeshAgent.SetDestination(destination);
    }

    public virtual void LookDelta(float pitch, float yaw)
    {
        if (pitch == 0f && yaw == 0f) return;

        if (HeadTransform)
            HeadTransform.localEulerAngles = new Vector3(Utility.ClampAngle(HeadTransform.localEulerAngles.x + pitch, -80f, 80f), 0f, 0f);

        transform.Rotate(new Vector3(0f, yaw, 0f));
    }

    public virtual void Equip(Equipable item)
    {
        if (_equipedItem)
            if (OnUnequip != null)
                OnUnequip(this, _equipedItem);

        _equipedItem = item;

        if (_equipedItem)
            if (OnEquip != null)
                OnEquip(this, _equipedItem);
    }

    public virtual void UseEquipedItem()
    {
        if (_equipedItem)
        {
            IUsable usable = _equipedItem as IUsable;

            if (usable != null)
                usable.Use(this);
        }
    }

    public virtual void UseWorldObject()
    {
        Ray ray = new Ray(HeadTransform.position, HeadTransform.forward);
        RaycastHit[] hits = Physics.RaycastAll(ray, 2f);
        RaycastHit nearestHit;

        if (Utility.NearestHitIgnoreObjects(hits, out nearestHit, ray.origin, _ownColliders))
        {
            IUsable usable = nearestHit.collider.GetComponent<IUsable>();
            if (usable != null)
            {
                usable.Use(this);
            }
        }
    }

    public void ApplyDamage(float amount, object instigator, DamageType damageType)
    {
        Health -= (int)amount;

        if (OnTakeDamage != null) OnTakeDamage(this, instigator, damageType);

        if (Health <= 0)
            Kill(instigator, damageType);
    }

    public void Kill(object instigator, DamageType damageType)
    {
        if (OnKilled != null) OnKilled(this, instigator, damageType);

        if (DestroyOnKilled)
            GameObject.Destroy(gameObject);
    }
}
