﻿using UnityEngine;
using System.Collections;

public interface IDamageable
{
    void ApplyDamage(float amount, object instigator, DamageType damageType);
    void Kill(object instigator, DamageType damageType);
}

public enum DamageType
{
    Generic,
    Projectile,
    Trigger_Hurt,
}
