﻿using UnityEngine;
using System.Collections;

public class AiWaypoint : MonoBehaviour
{
    public AiWaypoint[] LinkedWaypoints;
    public float ReachSize;

    void OnDrawGizmos()
    {
        if (LinkedWaypoints == null) return;

        for (int i = 0; i < LinkedWaypoints.Length; i++)
        {
            if (LinkedWaypoints[i])
                Gizmos.DrawLine(transform.position, LinkedWaypoints[i].transform.position);
        }
    }
}
