﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AiController : PawnController
{
    protected AiPerception _perception;
    public AiPerception Perception
    {
        get { return _perception; }
    }

    private AiPOI[] _aiPointsOfInterest;
    private AiPOI _currentPointOfInterest;

    private float _fPoiRefreshTimer = 0f;
    private const float _fPoiRefreshInterval = 0.5f;

    private AiState _currentAiState = AiState.Patrolling;
    public AiState CurrentAiState
    {
        get { return _currentAiState; }
    }
    public AiState DebugAiState;

    public AiWaypoint NextWaypoint;
    public AiPOI AttackTarget;
    public float _fAwareness = 0f;
    private Vector3 _vInvestigatePosition;
    private float _fWaitAtWaypointTimer;

    protected override void Awake()
    {
        base.Awake();

        _perception = GetComponentInChildren<AiPerception>();
    }

    protected override void Start()
    {
        base.Start();

        SetAiState(CurrentAiState);
    }

    protected override void DoControlTick()
    {
        base.DoControlTick();

        DebugAiState = CurrentAiState;

        UpdatePoi();

        switch (CurrentAiState)
        {
            case AiState.Idle:
                State_Idle();
                break;

            case AiState.Patrolling:
                State_Patrolling();
                break;

            case AiState.Investigating:
                State_Investigating();
                break;

            case AiState.Attacking:
                State_Attacking();
                break;

            case AiState.Fleeing:
                State_Fleeing();
                break;
        }

        HandlePerception();
    }

    private void State_Fleeing()
    {

    }

    private void State_Attacking()
    {
        if (Perception.CheckLineOfSight(AttackTarget))
        {
            transform.LookAt(AttackTarget.transform.position);
            transform.localEulerAngles = new Vector3(0f, transform.localEulerAngles.y, 0f);
            PossessedPawn.EquipedItem.transform.LookAt(AttackTarget.transform.position);
            PossessedPawn.UseEquipedItem();
        }
        else
        {
            SetAiState(AiState.Investigating);
            if (AttackTarget)
                _vInvestigatePosition = AttackTarget.transform.position;
        }

        if (_fAwareness <= 0f)
        {
            SetAiState(AiState.Patrolling);
            AttackTarget = null;
        }
    }

    private void State_Investigating()
    {
        if (Vector3.Distance(transform.position, _vInvestigatePosition) <= 1f)
            SetAiState(AiState.Patrolling);

        if (_fAwareness <= 0f)
        {
            SetAiState(AiState.Patrolling);
            AttackTarget = null;
        }
    }

    private void State_Patrolling()
    {
        if (NextWaypoint)
        {
            if (Vector3.Distance(transform.position, NextWaypoint.transform.position) < NextWaypoint.ReachSize)
            {
                _fWaitAtWaypointTimer -= Time.deltaTime;

                if (NextWaypoint.LinkedWaypoints.Length > 0)
                {
                    if (_fWaitAtWaypointTimer <= 0f)
                    {
                        _fWaitAtWaypointTimer = Random.Range(0f, 5f);
                        NextWaypoint = NextWaypoint.LinkedWaypoints[Random.Range(0, NextWaypoint.LinkedWaypoints.Length)];
                        PossessedPawn.MoveTo(NextWaypoint.transform.position);
                    }
                }
                else
                    SetAiState(AiState.Idle);
            }
        }
        else
            SetAiState(AiState.Idle);
    }

    private void State_Idle()
    {

    }

    private void SetAiState(AiState aiState)
    {
        _currentAiState = aiState;

        switch (aiState)
        {
            case AiState.Idle:
                PossessedPawn.CurrentSpeed = PossessedPawn.WalkSpeed;
                break;

            case AiState.Patrolling:
                PossessedPawn.CurrentSpeed = PossessedPawn.WalkSpeed;

                if (NextWaypoint)
                    PossessedPawn.MoveTo(NextWaypoint.transform.position);
                break;

            case AiState.Investigating:
                PossessedPawn.CurrentSpeed = PossessedPawn.WalkSpeed;

                PossessedPawn.MoveTo(_vInvestigatePosition);
                break;

            case AiState.Attacking:
                PossessedPawn.CurrentSpeed = PossessedPawn.RunSpeed;

                break;

            case AiState.Fleeing:
                PossessedPawn.CurrentSpeed = PossessedPawn.RunSpeed;
                break;
        }
    }

    private void UpdatePoi()
    {
        if (_fPoiRefreshTimer > 0f)
        {
            _fPoiRefreshTimer -= Time.deltaTime;
        }
        else
        {
            _fPoiRefreshTimer = _fPoiRefreshInterval;
            _aiPointsOfInterest = FindObjectsOfType<AiPOI>();
        }
    }

    private void HandlePerception()
    {
        if (!_perception) return;

        bool gettingSpooked = false;

        for (int i = 0; i < _aiPointsOfInterest.Length; i++)
        {
            if (Perception.Perceive(_aiPointsOfInterest[i]))
            {
                gettingSpooked = true;

                if (_aiPointsOfInterest[i].AttackMe)
                    AttackTarget = _aiPointsOfInterest[i];
            }
        }           

        if (gettingSpooked)
            Spook();
        else if (_fAwareness > 0f)
        {
            _fAwareness -= Time.deltaTime * 0.05f;
        }
    }

    private void Spook()
    {
        if (_fAwareness <= 1f)
            _fAwareness += Time.deltaTime;

        if (_fAwareness >= 1f)
        {
            if (AttackTarget)
                SetAiState(AiState.Attacking);
            else
                SetAiState(AiState.Investigating);
        }
    }
}


public enum AiState
{
    Idle,
    Patrolling,
    Investigating,
    Attacking,
    Fleeing,
}