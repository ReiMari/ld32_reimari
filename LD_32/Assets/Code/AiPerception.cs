﻿using UnityEngine;
using System.Collections;

public class AiPerception : MonoBehaviour
{
    [Header("Vision")]
    public float Fov = 80f;
    public float ViewRange = 100f;

    public bool Perceive(AiPOI poi)
    {
        if (!poi.Active) return false;

        if (CheckLineOfSight(poi))
            return true;

        return false;
    }

    public bool CheckLineOfSight(AiPOI target)
    {
        if (!target ||
            !target.IsVisible ||
            target.Luminance <= 0.3f)
            return false;

        Vector3 direction = Vector3.Normalize(target.transform.position - transform.position);

        float dot = Vector3.Dot(transform.forward, direction);

        // Not within view cone
        if (dot * 360f < Fov) return false;

        Ray ray = new Ray(transform.position, direction);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, ViewRange))
        {
            if (hit.collider == target.Collider)
            {
                return true;
            }
        }

        return false;
    }
}
