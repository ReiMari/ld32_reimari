﻿using UnityEngine;
using System.Collections;

public class PawnController : MonoBehaviour
{
    public bool AutoPossess = true;

    private Pawn _possessedPawn;
    public Pawn PossessedPawn
    {
        get { return _possessedPawn; }
    }

	protected virtual void Awake()
    {
        if (AutoPossess)
        {
            Pawn pawn = GetComponent<Pawn>();
            if (pawn)
            {
                pawn.Possess(this);
                _possessedPawn = pawn;
            }
        }
    }

    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {
        // Only update if we're possessing a pawn
        if (PossessedPawn)
            DoControlTick();
    }

    protected virtual void DoControlTick()
    {

    }
}
