﻿using UnityEngine;
using System.Collections;

public class AiPOI : MonoBehaviour
{
    public Pawn BoundPawn;

    public bool NeedsPerception = true;
    public bool Active = true;
    public bool AttackMe = false;
    public bool IsVisible = true;
    public float Luminance = 0f;

    private float _fLuminanceSampleTimer = 0f;
    private const float _fLuminanceSampleInterval = 0.1f;

    private Collider _collider;
    public Collider Collider
    {
        get { return _collider; }
    }
    
    void Awake()
    {
        _collider = GetComponent<Collider>();
        BoundPawn = GetComponent<Pawn>();
    }

    void Update()
    {
        _fLuminanceSampleTimer -= Time.deltaTime;
        if (_fLuminanceSampleTimer <= 0f)
            SampleLuminance();
    }

    private void SampleLuminance()
    {
        _fLuminanceSampleTimer = _fLuminanceSampleInterval;

        Light[] lights = FindObjectsOfType<Light>();

        Luminance = 0f;
        for (int i = 0; i < lights.Length; i++)
        {
            if (!lights[i].enabled || lights[i].intensity == 0f)
                continue;

            float distanceToLight = Vector3.Distance(lights[i].transform.position, transform.position);

            if (distanceToLight > lights[i].range)
                continue;

            Ray ray = new Ray(lights[i].transform.position, Vector3.Normalize(transform.position - lights[i].transform.position));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, lights[i].range))
            {
                AiPOI hitPoi = hit.collider.GetComponent<AiPOI>();
                if (!hitPoi || hitPoi != this)
                {
                    continue;
                }
            }

            Luminance += Mathf.Clamp01(1f - (distanceToLight / lights[i].range)) * lights[i].intensity;

            //Luminance += (1f / Mathf.Pow(Vector3.Distance(lights[i].transform.position, transform.position), 2f)) *
            //    lights[i].intensity * 10f;
        }
    }
}
