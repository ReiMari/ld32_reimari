﻿using UnityEngine;
using System.Collections;

public class Gun : Equipable, IUsable
{
    public Transform MuzzleTransform;
    public GameObject MuzzleFlashEffect;

    public float RefireInterval;
    private float _fRefireTimer = 0f;

    public float DamagePerBullet;

    public void Use(Pawn user)
    {
        if (_fRefireTimer <= 0f)
            Fire();
    }

    private void Fire()
    {
        _fRefireTimer = RefireInterval;

        if (MuzzleFlashEffect)
            GameObject.Instantiate(MuzzleFlashEffect, MuzzleTransform.position, MuzzleTransform.rotation);

        Ray ray = new Ray(MuzzleTransform.position, MuzzleTransform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 1000f))
        {
            HurtCollision hurt = hit.collider.GetComponent<HurtCollision>();
            if (hurt && hurt.Damageable != null)
                hurt.Damageable.ApplyDamage(DamagePerBullet, null, DamageType.Projectile);
        }
    }

    protected override void Update()
    {
        base.Update();

        if (_fRefireTimer > 0f)
            _fRefireTimer -= Time.deltaTime;
    }
}
