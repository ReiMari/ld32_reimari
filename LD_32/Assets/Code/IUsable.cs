﻿using UnityEngine;
using System.Collections;

public interface IUsable
{
    void Use(Pawn user);
}
