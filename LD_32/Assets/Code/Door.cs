﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour, IUsable
{
    private Animator _animator;
    private bool _lIsOpen = false;

    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    public void Use(Pawn user)
    {
        if (_animator)
        {
            _lIsOpen = !_lIsOpen;

            if (_lIsOpen)
            {
                _animator.ResetTrigger("Close");
                _animator.SetTrigger("Open");
            }
            else
            {
                _animator.ResetTrigger("Open");
                _animator.SetTrigger("Close");
            }
        }
    }
}
