﻿using UnityEngine;
using System.Collections;

public class PlayerController : PawnController
{
    public float HorizontalSensitivity;
    public float VerticalSensitivity;
    public bool InvertVertical = false;
    public bool InputEnabled = true;

    protected override void Awake()
    {
        base.Awake();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    protected override void DoControlTick()
    {
        base.DoControlTick();

        HandleMovement();

        HandleUse();
    }

    private void HandleUse()
    {
        if (!InputEnabled) return;

        if (Input.GetKeyDown(KeyCode.F))
            PossessedPawn.UseEquipedItem();

        if (Input.GetKeyDown(KeyCode.E))
            PossessedPawn.UseWorldObject();
    }

    private void HandleMovement()
    {
        if (!InputEnabled) return;

        PossessedPawn.MoveForward(Input.GetAxis("Vertical"));
        PossessedPawn.MoveRight(Input.GetAxis("Horizontal"));

        PossessedPawn.LookDelta(
            Input.GetAxis("Mouse Y") * VerticalSensitivity * (InvertVertical ? 1f : -1f),
            Input.GetAxis("Mouse X") * HorizontalSensitivity);

        PossessedPawn.CurrentSpeed = Input.GetKey(KeyCode.LeftShift) ? PossessedPawn.RunSpeed : PossessedPawn.WalkSpeed;
    }
}
