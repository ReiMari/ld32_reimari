﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public static class Utility
{
	public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < 90 || angle > 270)
        {
            if (angle > 180) angle -= 360;
            if (max > 180) max -= 360;
            if (min > 180) min -= 360;
        }

        angle = Mathf.Clamp(angle, min, max);
        if (angle < 0) angle += 360;

        return angle;
    }

    public static bool NearestHitIgnoreObjects(RaycastHit[] hits, out RaycastHit nearestHit, Vector3 rayOrigin, params Collider[] ignoredColliders)
    {
        int nearestIndex = -1;
        float nearestDistance = float.MaxValue;

        for (int i = 0; i < hits.Length; i++)
        {
            if (ignoredColliders.Contains(hits[i].collider)) continue;

            float distance = Vector3.Distance(rayOrigin, hits[i].point);

            if (distance < nearestDistance)
            {
                nearestIndex = i;
                nearestDistance = distance;
            }
        }

        if (nearestIndex == -1)
        {
            nearestHit = new RaycastHit();
            return false;
        }

        nearestHit = hits[nearestIndex];

        return true;
    }
}
