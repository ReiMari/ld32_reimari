﻿using UnityEngine;
using System.Collections;

public class DistantSniper : MonoBehaviour
{
    private FlashLight _flashlight;
    private SniperTarget _target;
    private float _fTargetMaintainTimer = 0.5f;
    private float _fTargetMaintainTime = 0.5f;
    private Gun _gun;
    public GameObject Laser;
    private float _fHoldFireTimer = 1f;
    public Collider[] IgnoredColliders;

    void Awake()
    {
        _gun = GetComponent<Gun>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
            Application.LoadLevel(Application.loadedLevel);

        if (!_flashlight)
            _flashlight = FindObjectOfType<FlashLight>();

        if (_flashlight)
        {
            if (_flashlight.SniperTarget)
            {
                _fTargetMaintainTimer = _fTargetMaintainTime;
                _target = _flashlight.SniperTarget;
            }
            else
            {
                _fTargetMaintainTimer -= Time.deltaTime;

                if (_fTargetMaintainTimer <= 0f)
                    _target = null;
            }
        }

        if (_target)
        {
            Laser.SetActive(true);

            transform.rotation = Quaternion.Slerp(
                transform.rotation,
                Quaternion.LookRotation(Vector3.Normalize((_target.transform.position + Vector3.up * _target.AimHeightOffset) - transform.position)),
                Time.deltaTime * 6f);

            Ray ray = new Ray(transform.position, transform.forward);
            RaycastHit[] hits = Physics.RaycastAll(ray, 1000f);
            RaycastHit hit;

            if (Utility.NearestHitIgnoreObjects(hits, out hit, ray.origin, IgnoredColliders))
            {
                if (hit.collider.GetComponent<SniperTarget>())
                {
                    _fHoldFireTimer -= Time.deltaTime;

                    if (_fHoldFireTimer <= 0f)
                    {
                        _fHoldFireTimer = 1f;
                        _gun.Use(null);
                    }

                    Laser.transform.localScale = new Vector3(1f, 1f, hit.distance);
                }
            }
            else
                Laser.transform.localScale = new Vector3(1f, 1f, 1000f);
        }
        else
        {
            Laser.SetActive(false);
            transform.rotation = Quaternion.LookRotation(-Vector3.up);
        }
    }
}
