﻿using UnityEngine;
using System.Collections;

public class DestroyAfter : MonoBehaviour
{
    public float Timer = 1f;

	void Update()
    {
        if (Timer <= 0f)
            Destroy(gameObject);

        Timer -= Time.deltaTime;
    }
}
