﻿using UnityEngine;
using System.Collections;

public class Damageable : MonoBehaviour, IDamageable
{
    public float Health = 1;
    public GameObject KillEffect;

    void Awake()
    {
        HurtCollision hurtSingle = GetComponent<HurtCollision>();
        if (hurtSingle)
            hurtSingle.Damageable = this;

        HurtCollision[] hurt = GetComponentsInChildren<HurtCollision>();
        for (int i = 0; i < hurt.Length; i++)
            hurt[i].Damageable = this;
    }

    public void ApplyDamage(float amount, object instigator, DamageType damageType)
    {
        Health -= amount;

        if (Health <= 0f) Kill(instigator, damageType);
    }

    public void Kill(object instigator, DamageType damageType)
    {
        if (KillEffect)
            GameObject.Instantiate(KillEffect, transform.position, transform.rotation);

        GameObject.Destroy(gameObject);
    }
}
