﻿using UnityEngine;
using System.Collections;

public class Usable : MonoBehaviour, IUsable
{
    public delegate void UsableEvent(Usable sender, params object[] args);
    public UsableEvent OnUse;

    public GameObject[] UsableSubTargets;

    public void Use(Pawn user)
    {
        for (int i = 0; i < UsableSubTargets.Length; i++)
        {
            IUsable usableSub = UsableSubTargets[i].GetComponent<IUsable>();
            if (usableSub != null)
                usableSub.Use(user);
        }

        if (OnUse != null) OnUse(this, user);
    }
}
