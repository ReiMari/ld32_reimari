﻿using UnityEngine;
using System.Collections;

public class Crosshair : MonoBehaviour
{
    public void SetPosition(Vector3 point)
    {
        Vector3 world = Camera.main.WorldToScreenPoint(point);
        transform.localPosition = new Vector3(world.x, world.y, 0f);
    }
}
