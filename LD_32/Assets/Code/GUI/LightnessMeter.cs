﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LightnessMeter : MonoBehaviour
{
    public Image FillBar;
    public AiPOI TargetPoi;
    private float _fFillValue;

    void Update()
    {
        _fFillValue = Mathf.Lerp(_fFillValue, TargetPoi.Luminance, Time.deltaTime * 5f);
        FillBar.fillAmount = _fFillValue;
    }
}
