﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour
{
    public Pawn TargetPawn;
    public string StringFormat = "{0}";
    public Text Text;

    void OnEnable()
    {
        if (TargetPawn)
        {
            TargetPawn.OnHealthChanged += pawn_OnHealthChanged;
        }
    }

    void OnDisable()
    {
        if (TargetPawn)
            TargetPawn.OnHealthChanged -= pawn_OnHealthChanged;
    }

    private void pawn_OnHealthChanged(Pawn sender, params object[] args)
    {
        UpdateText();
    }

    public void UpdateText()
    {
        if (Text)
            Text.text = string.Format(StringFormat, TargetPawn.Health);
    }
}
