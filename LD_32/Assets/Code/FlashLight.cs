﻿using UnityEngine;
using System.Collections;

public class FlashLight : Equipable, IUsable
{
    private Light _light;
    private bool _lEnabled;
    private AiPOI _aiPoi;
    public SniperTarget SniperTarget;
    public Crosshair crosshair;

    protected override void Awake()
    {
        base.Awake();

        _light = GetComponent<Light>();
        if (!_light)
            _light = GetComponentInChildren<Light>();

        _aiPoi = GetComponent<AiPOI>();
        if (!_aiPoi)
            _aiPoi = GetComponentInChildren<AiPOI>();
    }

    protected override void Start()
    {
        base.Start();

        if (_light)
            _light.enabled = _lEnabled;
    }

    protected override void Update()
    {
        base.Update();

        UpdateLightPoi();

        UpdateTarget();

        UpdateCrosshair();
    }

    private void UpdateCrosshair()
    {
        Ray ray = new Ray(_light.transform.position, _light.transform.forward);
        RaycastHit[] hits = Physics.RaycastAll(ray, _light.range * 0.8f);

        if (hits.Length > 0)
        {
            RaycastHit nearest;
            if (Utility.NearestHitIgnoreObjects(hits, out nearest, ray.origin, _aiPoi.Collider))
            {
                if (crosshair)
                    crosshair.SetPosition(nearest.point);
            }
        }
        else
        {
            crosshair.SetPosition(transform.position + transform.forward * _light.range * 0.8f);
        }
    }

    private void UpdateTarget()
    {
        if (!_lEnabled) return;

        Ray ray = new Ray(_light.transform.position, _light.transform.forward);
        RaycastHit[] hits = Physics.RaycastAll(ray, _light.range * 0.8f);

        SniperTarget = null;
        if (hits.Length > 0)
        {
            RaycastHit nearest;
            if (Utility.NearestHitIgnoreObjects(hits, out nearest, ray.origin, _aiPoi.Collider))
            {
                SniperTarget = nearest.collider.GetComponent<SniperTarget>();
            }
        }
    }

    private void UpdateLightPoi()
    {
        if (!_aiPoi) return;

        _aiPoi.IsVisible = this._lEnabled;

        // TODO: Update POI position
        Ray ray = new Ray(_light.transform.position, _light.transform.forward);
        RaycastHit[] hits = Physics.RaycastAll(ray, _light.range * 0.8f);

        if (hits.Length > 0)
        {
            RaycastHit nearest;
            if (Utility.NearestHitIgnoreObjects(hits, out nearest, ray.origin, _aiPoi.Collider))
            {
                _aiPoi.transform.position = nearest.point + nearest.normal * 0.1f;
            }
        }
    }

    public void Toggle()
    {
        _lEnabled = !_lEnabled;

        if (_light)
            _light.enabled = _lEnabled;
    }

    public void Use(Pawn user)
    {
        Toggle();
    }
}
